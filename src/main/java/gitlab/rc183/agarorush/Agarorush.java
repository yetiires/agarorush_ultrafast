package gitlab.rc183.agarorush;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gitlab.rc183.agarorush.event.ItemEntityCollisionCallback;
import gitlab.rc183.agarorush.event.PlayerEndPortalCollisionCallback;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.event.player.AttackEntityCallback;
import net.fabricmc.fabric.api.event.player.PlayerBlockBreakEvents;
import net.minecraft.block.Blocks;
import net.minecraft.block.GravelBlock;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.Text;
import net.minecraft.text.Texts;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Formatting;
import net.minecraft.world.GameMode;
import net.minecraft.world.World;

public class Agarorush implements ModInitializer {
	
	private final List<Enchantment> ENCHANTS = Arrays.asList(Enchantments.SHARPNESS, Enchantments.POWER, Enchantments.PROTECTION, Enchantments.EFFICIENCY, Enchantments.UNBREAKING, Enchantments.FLAME, Enchantments.INFINITY);
	private PlayerEntity winner = null;
	
	@Override
	public void onInitialize() {
		System.out.println("Mod Agarorush activé!");
		
		PlayerBlockBreakEvents.BEFORE.register((world, player, pos, state, entity) -> {
			// DROP ENCHANTED BOOK 1/64
			if(state.getBlock()==Blocks.REDSTONE_ORE||state.getBlock()==Blocks.COAL_ORE
					||state.getBlock()==Blocks.DIAMOND_ORE||state.getBlock()==Blocks.EMERALD_ORE
					||state.getBlock()==Blocks.GOLD_ORE||state.getBlock()==Blocks.IRON_ORE
					||state.getBlock()==Blocks.LAPIS_ORE||state.getBlock()==Blocks.NETHER_GOLD_ORE
					||state.getBlock()==Blocks.NETHER_QUARTZ_ORE) {
				if(world.random.nextInt(64)==0) {
					ItemStack stack = new ItemStack(Items.ENCHANTED_BOOK);
					Enchantment ench = ENCHANTS.get(world.random.nextInt(ENCHANTS.size()));
					int enchL = world.random.nextInt(15)+1;
					Map<Enchantment, Integer> enchantments = new HashMap<Enchantment, Integer>();
					enchantments.put(ench, enchL);
					EnchantmentHelper.set(enchantments, stack);
					ItemEntity itemEntity = new ItemEntity(world, pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5, stack);
				    world.spawnEntity(itemEntity);
				    Text enchN = enchL > 10 ? ench.getName(0).copy().append(" "+enchL) : ench.getName(enchL).shallowCopy().formatted(Formatting.AQUA);
				    for(PlayerEntity p : world.getPlayers()) {
				    	p.sendMessage(player.getDisplayName().shallowCopy().append(" got the enchanted book ").shallowCopy().append(Texts.bracketed(enchN).formatted(Formatting.AQUA)), false); //new TranslatableText("agarorush.chat.book", new Object[] {player.getDisplayName(), Texts.bracketed(enchN).formatted(Formatting.AQUA)})
						if (!world.isClient) world.playSound(null, p.getBlockPos(), SoundEvents.BLOCK_NOTE_BLOCK_CHIME, SoundCategory.BLOCKS, 1f, 2f);
				    }
				}
			}
			
			// DROP BOW IF !INVENTORY CONTAINS BOW
			if(!(state.getBlock() instanceof GravelBlock)) return true;
			for(ItemStack item : player.inventory.main) {
				if(item.getItem() instanceof BowItem) return true;
			}
			world.removeBlock(pos, false);
			ItemStack stack = new ItemStack(Items.BOW);
		    ItemEntity itemEntity = new ItemEntity(world, pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5, stack);
		    world.spawnEntity(itemEntity);
			return false;
		});
		
		ItemEntityCollisionCallback.EVENT.register((item, with) -> {
			
			// MERGE ON GROUND
			
			if((!item.getStack().hasEnchantments() && item.getStack().getItem() != Items.ENCHANTED_BOOK) && (!with.getStack().hasEnchantments() && item.getStack().getItem() != Items.ENCHANTED_BOOK)) return true;
			ItemStack itemS = item.getStack();
			ItemStack withS = with.getStack();
			if(itemS.getItem() == withS.getItem()) {
				if(!EnchantmentHelper.get(itemS).isEmpty() && !EnchantmentHelper.get(withS).isEmpty()) {
					EnchantmentHelper.set(combine(EnchantmentHelper.get(itemS), EnchantmentHelper.get(withS)), itemS);
					with.remove();
					if (!item.world.isClient) item.world.playSound(null, item.getBlockPos(), SoundEvents.BLOCK_ENCHANTMENT_TABLE_USE, SoundCategory.BLOCKS, 0.5f, 1f);
					return true;
				}
			}
			if(itemS.getItem()==Items.ENCHANTED_BOOK) {
				if(withS.getItem().isEnchantable(itemS)) {
					EnchantmentHelper.set(combine(EnchantmentHelper.get(withS), EnchantmentHelper.get(itemS)), withS);
					item.remove();
					if (!item.world.isClient) item.world.playSound(null, with.getBlockPos(), SoundEvents.BLOCK_ENCHANTMENT_TABLE_USE, SoundCategory.BLOCKS, 0.5f, 1f);
					return true;
				}
			}
			if(withS.getItem()==Items.ENCHANTED_BOOK) {
				if(itemS.getItem().isEnchantable(itemS)) {
					EnchantmentHelper.set(combine(EnchantmentHelper.get(itemS), EnchantmentHelper.get(withS)), itemS);
					with.remove();
					if (!item.world.isClient) item.world.playSound(null, item.getBlockPos(), SoundEvents.BLOCK_ENCHANTMENT_TABLE_USE, SoundCategory.BLOCKS, 0.5f, 1f);
					return true;
				}
			}
		 
			return true;
		});
		
		AttackEntityCallback.EVENT.register((player, world, hand, entity, hitResult) -> {
			// PVP DISABLED IN OVERWORLD
			if(!player.isSpectator() && !entity.isSpectator()) {
				if(player != entity) {
					if(entity instanceof PlayerEntity) {
						if(world.getDimension().isBedWorking()) return ActionResult.FAIL;
					}
				}
			}
			return ActionResult.PASS;
		});
		
		PlayerEndPortalCollisionCallback.EVENT.register((player, world, pos, state) -> {
			if(world.getRegistryKey()==World.END) {
				if(this.winner==null) {
					this.winner=player;
					player.getServer().getPlayerManager().getPlayerList().forEach(p->{
						p.teleport(p.getX(), p.getY()+1, p.getZ());
						p.setVelocity(0, 5, 0);
						p.setGameMode(GameMode.SPECTATOR);
						p.sendMessage(winner.getName().shallowCopy().append(" is the winner!").formatted(Formatting.YELLOW), false);
						if (!player.world.isClient) player.world.playSound(null, p.getBlockPos(), SoundEvents.BLOCK_BELL_USE, SoundCategory.BLOCKS, 1.5f, 0.5f);
					});
				}
				return false;
			}
			return true;
		});
	}
	
	private Map<Enchantment, Integer> combine(Map<Enchantment, Integer> first, Map<Enchantment, Integer> second){
		Map<Enchantment, Integer> last = first;
		for (Enchantment enchantment : second.keySet()) {
			if (enchantment == null) {
				continue;
			}
			int t = ((Integer)last.getOrDefault(enchantment, Integer.valueOf(0))).intValue();
			int u = ((Integer)second.get(enchantment)).intValue();
			u = (t == u) ? (u + 1) : Math.max(u, t);
	          
			/*if (u > enchantment.getMaxLevel()) {    // FOR THE AGARORUSH, WE ACCEPT THE COMBINATION OF
				u = enchantment.getMaxLevel();        // IMPOSSIBLE ENCHANTS IN VANILLA (example: sharpness 5 + sharpness 5 = sharpness 6)
			}*/                                       // THIS IS WHY THESE LINES ARE COMMENTED
			
			last.put(enchantment, Integer.valueOf(u));
	    } 
		return last;
	}
}
