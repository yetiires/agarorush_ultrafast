package gitlab.rc183.agarorush.event;

import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public interface PlayerEndPortalCollisionCallback {
	
	Event<PlayerEndPortalCollisionCallback> EVENT = EventFactory.createArrayBacked(PlayerEndPortalCollisionCallback.class,
            (listeners) -> (player, world, pos, state) -> {
                for (PlayerEndPortalCollisionCallback listener : listeners) {
                    boolean result = listener.interact(player, world, pos, state);
     
                    if(!result) {
                        return result;
                    }
                }
            return true;
        });
     
        boolean interact(PlayerEntity player, World world, BlockPos pos, BlockState state);
        
}
