package gitlab.rc183.agarorush.event;

import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;
import net.minecraft.entity.ItemEntity;

public interface ItemEntityCollisionCallback {
	
    Event<ItemEntityCollisionCallback> EVENT = EventFactory.createArrayBacked(ItemEntityCollisionCallback.class,
            (listeners) -> (item, with) -> {
                for (ItemEntityCollisionCallback listener : listeners) {
                    boolean result = listener.interact(item, with);
     
                    if(!result) {
                        return result;
                    }
                }
            return true;
        });
     
        boolean interact(ItemEntity item, ItemEntity with);

}
