package gitlab.rc183.agarorush.event.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import gitlab.rc183.agarorush.event.PlayerEndPortalCollisionCallback;
import net.minecraft.block.BlockState;
import net.minecraft.block.EndPortalBlock;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/*@Mixin(Entity.class)
public class PlayerWorldMoveMixin {
	
	@Inject(method = "moveToWorld", at = @At("INVOKE"))
    private void onMoveToWorld(ServerWorld destination, CallbackInfoReturnable<Boolean> info) {
		System.out.println("player moved to another world");
		if(((Entity)((Object) this)) instanceof PlayerEntity) {
			boolean result = PlayerWorldMoveCallback.EVENT.invoker().interact((PlayerEntity) (Object) this, destination);
	        
	        if(!result) {
	            info.cancel();
	        }
		}
    }
	
}*/

@Mixin(EndPortalBlock.class)
public class PlayerEndPortalCollisionMixin {
	
	@Inject(method = "onEntityCollision", at = @At("INVOKE"))
    private void onPlayerCollision(BlockState state, World world, BlockPos pos, Entity entity, CallbackInfo info) {
		if(entity instanceof PlayerEntity) {
			boolean result = PlayerEndPortalCollisionCallback.EVENT.invoker().interact((PlayerEntity)entity, world, pos, state);
	        
	        if(!result) {
	            return;
	        }
		}
    }
	
}