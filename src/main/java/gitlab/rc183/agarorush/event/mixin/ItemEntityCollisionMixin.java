package gitlab.rc183.agarorush.event.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import gitlab.rc183.agarorush.event.ItemEntityCollisionCallback;
import net.minecraft.entity.Entity;
import net.minecraft.entity.ItemEntity;

@Mixin(Entity.class)
public class ItemEntityCollisionMixin {
 
	@Inject(method = "collidesWith", at = @At("HEAD"))
    private void onCollide(final Entity other, final CallbackInfoReturnable<Boolean> info) {
		if((((Entity)((Object) this)) instanceof ItemEntity) && (other instanceof ItemEntity)) {
			boolean result = ItemEntityCollisionCallback.EVENT.invoker().interact((ItemEntity) (Object) this, (ItemEntity)other);
	        
	        if(!result) {
	            info.cancel();
	        }
		}
    }
	
}