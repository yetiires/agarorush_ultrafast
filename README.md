# Agarorush Ultrafast

Minecraft mod implementing the Agarorush Ultrafast (concept by AgarOther)

Requires fabric API

> - Pvp disabled in the Overworld
> - Items can be merged on the ground with another item of the same type or with a book like an anvil
> - 1/64 to obtain an enchanted book from level 1 to 15 by mining an ore (sharpness, power, protection, efficiency, unbreaking, flame, infinity)
> - coal ore: 1 enderpearl or 1 coal
> - diamond ore: netherite ingot or enchanting table or diamond sword or diamond helmet or diamond chestplate or diamond leggings or diamond boots or ender eye or diamond pickaxe or trident loyalty III
> - emerald ore: enchanted golden apple
> - gold ore: 16 cooked beef
> - iron ore: between 1 and 3 iron ingots
> - nether gold ore: 1 blaze powder
> - redstone ore: 3 obsidians
> - gravel: 1 bow then 12 arrows, 1/10 to get 1 flint
> - coal block: 6 iron ingots
> - diamond block: 1 totem of undying
> - gold block: 1 golden apple
> - iron block: 6 diamonds
> - lapis block: 1 netherite ingot

The first to enter the portal after defeating the dragon wins!